from random import randint as ri
import curses as c
rcd={0x72:1,0x67:2,0x62:3,0x71:-1}
scr=c.initscr()
c.start_color()
c.noecho()
c.curs_set(0)
c.init_pair(1,1,1)
c.init_pair(2,2,2)
c.init_pair(3,4,4)
C=c.COLS-1
c.halfdelay(2)
cx,cy=C//2,c.LINES//2-1
l=[cx,cy][cx>cy]*2
aa=[(l//2,C//2+l,0,-1),(l//2,C//2-l,0,1),(0,C//2,1,0),(l,C//2,-1,0)]
ats,s,r,got=[],2,1,0
while r!=-1:
	for i in range(0,s+1):
		for j in range(0,2*s+1):
			scr.addstr(cy-s//2+i,cx-s+j,'#',c.color_pair(r))
	nats=[]
	for a in ats:
		na=(a[0]+a[2],a[1]+a[3],a[2],a[3],a[4])
		if na[0]<cy+s//2+1 and na[2]<0 or na[0]>cy-s//2-1 and na[2]>0 or na[1]<cx+s+1 and na[3]<0 or na[1]>cx-s-1 and na[3]>0:
			if r==a[4]:
				scr.addstr(a[0],a[1],' ')
				got+=1
			else:
				nats=[]
				break
		else:
			nats.append(na)
			scr.addstr(a[0],a[1],' ')
			scr.addstr(na[0],na[1],'#',c.color_pair(na[4]))
	ats=nats
	if len(ats)==0:
		if got==s//2:
			s+=2
			scr.addstr(cy,0,"Nice one")
			scr.getch()
		scr.clear()
		scr.addstr("r,g,b-colors\nq-quit\nlvl {}".format(s//2))
		ac=3
		got=0
	if ac!=0:
		ac=(ac+1)%(2*s)
		if ac%4==0:
			z=aa[ri(0,3)]
			ats.append((z[0],z[1],z[2],z[3],ri(1,3)))
	if cy-s//2<1 or cx-s<(C)//2-l-1:
		break
	r=rcd.get(scr.getch(),r)
c.echo()
c.endwin()
